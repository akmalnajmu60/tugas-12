<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (){
    return view('index');
});

Route::get('/register', 'HomeController@register');

Route::post('/welcome', 'AuthController@welcome');

Route::get('/master', function (){
    return view('adminlte.master');
});

Route::get('/table', function (){
    return view('adminlte.items.table');
});

Route::get('/data-table', function (){
    return view('adminlte.items.data-table');
});

Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
Route::get('/cast', 'CastController@index');