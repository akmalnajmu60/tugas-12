<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create(){
        return view('adminlte.cast.create');
    }

    public function store(request $request){

        $request->validate([
            'nama' => 'required|unique:post',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $query = DB ::table('cast')->insert([
            "Nama"=> $request["Nama"],
            "Umur"=> $request["Umur"],
            "Bio"=> $request["Bio"]
        ]);
        return redirect('/cast/create');
    }

    public function index(){
        $cast = DB::table('cast')->get();
        // dd($cast);
        return view('adminlte.cast.index', compact('cast'));
    }
}