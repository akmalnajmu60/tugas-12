<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function welcome(request $request)
    {
        $depan = $request['name'];

        return view('welcome', compact('depan'));
    }
}
